//
//  Constants.swift
//  SoftVisionExample
//
//  Created by Vijay on 6/6/17.
//  Copyright © 2017 exp. All rights reserved.
//

enum Constants {
    /// Url are declared here
    enum EndPoints {
        static let metadataURL = "https://evaluation-exercise.herokuapp.com/getMasterData"
        
        static let profileURL = "https://evaluation-exercise.herokuapp.com/profile"
    }
    
    /// App message are declared here.
    enum Messages {
        static let noInternet = "Kindly check your network connection"
        
        static let unsuccessfulFetch = "Sorry problem in fetching data"
        
        static let deleteSuccess = "details uploaded successfully"
    }
}

