//
//  DataAccessManager.swift
//  SoftVisionExample
//
//  Created by Vijay on 6/6/17.
//  Copyright © 2017 exp. All rights reserved.
//

import Foundation

class DataAccessManager: NSObject {
    
    static let manager = DataAccessManager()
    
    private override init() {
        
    }

    func fetchMetaData(completion: @escaping (MetaData? , Error?)->Void) {
        let url = URL(string: Constants.EndPoints.metadataURL)!
        
        let task = URLSession.shared.dataTask(with: URLRequest(url: url), completionHandler: { data,response,error in
            if error != nil{
                completion(nil,error)
                return
            }
            do {
                
                let parsedData = try JSONSerialization.jsonObject(with: data!) as? [String:Any]
                let masterData = parsedData?["MasterData"] as? [String:Any]
                let educationDetails = DataDeserializer.deserializeEducation(masterData: masterData?["EducationalQualification"] as! [[String : Any]])
                
                let jobDetails = DataDeserializer.deserializeJobType(jobTypes: masterData?["JobTypes"] as! [[String : Any]])
                let metaData = MetaData(education:educationDetails, job: jobDetails)
                completion(metaData, error)
            } catch let error as NSError {
                print(error)
            }
        })
        task.resume()
    }
}
