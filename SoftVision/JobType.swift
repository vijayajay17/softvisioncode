//
//  JobType.swift
//  SoftVisionExample
//
//  Created by Vijay on 6/6/17.
//  Copyright © 2017 exp. All rights reserved.
//

import Foundation

class JobType {
    var jobType:String?
    
    init?(jobType:String) {
        self.jobType = jobType
    }
    
}
