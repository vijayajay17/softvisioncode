//
//  MetaData.swift
//  SoftVisionExample
//
//  Created by Vijay on 6/6/17.
//  Copyright © 2017 exp. All rights reserved.
//

import Foundation

class MetaData {
    
    var education:[Education]?
    var job:[JobType]?
    
    init?(education:[Education], job: [JobType]) {
        self.education = education
        self.job = job
    }
}
