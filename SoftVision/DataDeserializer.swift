//
//  DataDeserializer.swift
//  SoftVisionExample
//
//  Created by Vijay on 6/6/17.
//  Copyright © 2017 exp. All rights reserved.
//

import Foundation
class DataDeserializer {
    
   class func deserializeEducation(masterData: [[String: Any]])  -> [Education] {
    var educationTypes:[Education] = []
    for edu in masterData {
        if let educationType = Education(educationType: edu["Qualification"] as! String){
            educationTypes.append(educationType)
        }
    }
    return educationTypes
}
    
   class func deserializeJobType(jobTypes: [[String:Any]]) -> [JobType] {
    var jobTypesArray:[JobType] = []
    for job in jobTypes {
        if let jobType = JobType(jobType: job["JobType"] as! String){
            jobTypesArray.append(jobType)
        }
    }
    return jobTypesArray
    }
}
